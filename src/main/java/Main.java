import static spark.Spark.post;

import java.io.IOException;
import java.time.Instant;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;

import freemarker.template.TemplateException;

public class Main {

	private static ExecutorService threadPool = Executors.newCachedThreadPool();

	public static void main(String[] args) throws Exception {

		Persistance.setObject("1", JobFactory.buildAccepted("1"));
		Persistance.setObject("2", JobFactory.buildFailed("2", "Error"));
		Persistance.setObject("3", JobFactory.buildRunning("3"));
		Persistance.setObject("4", JobFactory.buildSucceeded("4", "result"));

		post("/wps", (req, res) -> {
			
			MultipartConfigElement multipartConfigElement = new MultipartConfigElement("/tmp");
			req.raw().setAttribute("org.eclipse.multipartConfig", multipartConfigElement);
			Part file = req.raw().getPart("input"); // file is name of the upload
													// form

			String request = IOUtils.toString(file.getInputStream());
			
			log("Incoming request");

//			final String request = req.body();

			res.type("application/xml");

			if (isExecuteOperation(request)) {
				if (isAsyncOperation(request)) {
					log("Async execute");
					return doAsyncExecute(request);
				} else {
					Instant before = Instant.now();
					Object executeResult = doSyncExecute(request);
					Instant after = Instant.now();
					log("Sync execute done in %s ms", after.toEpochMilli() - before.toEpochMilli());
					return executeResult;
				}
			}
			if (isGetStatusOperation(request)) {
				return doGetStatus(request);
			}
			if (isGetResultOperation(request)) {
				return doGetResult(request);
			}

			return WPSResponseGenerator.exception("InvalidWPSOperation");
		});
	}

	private static Object doGetResult(final String request) throws TemplateException, IOException {
		String token = WPSRequestParser.extractTokenFromGetStatus(request);

		if (!Persistance.hasKey(token)) {
			return WPSResponseGenerator.exception("NoSuchJob");
		}

		Job job = Persistance.getObject(token, Job.class);

		if (!job.status.equals("Succeeded")) {
			return WPSResponseGenerator.exception("ResultNotReady");
		}

		String result = job.result;
		String outputId = "1";
		Instant expiration = Instant.now().plusSeconds(10 * 60);

		return WPSResponseGenerator.complexDataResult(token, expiration, outputId, result);
	}

	private static Object doGetStatus(final String request) throws TemplateException, IOException, Exception {
		String token = WPSRequestParser.extractTokenFromGetStatus(request);

		if (!Persistance.hasKey(token)) {
			return WPSResponseGenerator.exception("NoSuchJob");
		}

		Job job = Persistance.getObject(token, Job.class);

		return WPSResponseGenerator.statusInfo(token, job.status, Instant.now().plusSeconds(10));
	}

	private static Object doAsyncExecute(final String request) throws Exception {
		final String token = generateToken();

		Persistance.setObject(token, JobFactory.buildAccepted(request));

		// Fire up the execute request
		threadPool.submit(new Runnable() {
			@Override
			public void run() {
				try {
					WPSExecute.execute(request, token);
				} catch (Exception e) {
					e.printStackTrace();
					Persistance.setObject(token, JobFactory.buildFailed(token, e.getMessage()));
				}
			}
		});

		// Send back the token
		return WPSResponseGenerator.statusInfo(token, Persistance.getObject(token, Job.class).status,
				Instant.now().plusSeconds(10));
	}

	private static Object doSyncExecute(final String request) throws Exception {
		final String token = generateToken();
		final String result = WPSExecute.executeSync(request);
		final String outputId = "1";

		return WPSResponseGenerator.complexDataResult(token, Instant.now(), outputId, result);
	}

	private static boolean isExecuteOperation(String requestBody) {
		return requestBody.contains("<wps:Execute");
	}

	private static boolean isGetStatusOperation(String requestBody) {
		return requestBody.contains("<wps:GetStatus");
	}

	private static boolean isGetResultOperation(String requestBody) {
		return requestBody.contains("<wps:GetResult");
	}

	private static boolean isAsyncOperation(String requestBody) {
		return requestBody.contains("mode=\"async\"");
	}

	private static String generateToken() {
		return System.nanoTime() + "";
	}

	private static void log(String format, Object... args) {
		System.out.println(String.format(format, args));
	}

	// String request = FileUtils.readFileToString(new
	// File("/home/andrea/Projects/eclipse-mars/spark-test/src/main/resources/WPSRequestBody.xml"));

	// String testInputTxt = WPSClassificator.extractTestData(request);
	// String trainInputTxt = WPSClassificator.extractTrainData(request);
	//
	// WPSDataInput testDataInput =
	// WPSClassificator.parseDataInput(testInputTxt);
	// WPSDataInput trainDataInput =
	// WPSClassificator.parseDataInput(trainInputTxt);
	//
	// Instances testInstances =
	// WPSClassificator.createInstances(testDataInput);
	// Instances trainInstances =
	// WPSClassificator.createInstances(trainDataInput);
	//
	// testInstances.setClassIndex(testInstances.attribute(testDataInput.classIndex).index());
	// trainInstances.setClassIndex(trainInstances.attribute(trainDataInput.classIndex).index());
	//
	// Classifier ibk = new IBk();
	// ibk.buildClassifier(trainInstances);
	//
	// Evaluation evaluation = new Evaluation(trainInstances);
	// evaluation.evaluateModel(ibk, testInstances);
	//
	// System.out.println(evaluation.toSummaryString());
}