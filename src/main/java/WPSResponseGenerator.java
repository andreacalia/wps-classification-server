import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

public class WPSResponseGenerator {
	
	private static Configuration freemarkerCfg;

	static {
        try {
        	freemarkerCfg = new Configuration(Configuration.VERSION_2_3_22);
			freemarkerCfg.setDirectoryForTemplateLoading(new File(WPSResponseGenerator.class.getResource("tpl").getPath()));
			freemarkerCfg.setDefaultEncoding("UTF-8");
			freemarkerCfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		} catch (IOException e) {
			throw new RuntimeException("Sorry unable to init, tpl folder not found");
		}
	}
	
	public static String statusInfo(String jobId, String status, Instant nextPoll) throws Exception {
		
		String nextPollTxt = nextPoll.toString();
        
        Map<String, String> root = new HashMap<>();
        root.put("jobId", jobId);
        root.put("status", status);
        root.put("nextPoll", nextPollTxt);
        
        StringWriter sw = new StringWriter();
        
        Template temp = freemarkerCfg.getTemplate("asyncExecute.tpl");
        temp.process(root, sw);

		return sw.toString();		
	}
	
	public static String exception(String code) throws TemplateException, IOException {
		
		Map<String, String> root = new HashMap<>();
        root.put("code", code);
        
        StringWriter sw = new StringWriter();
        
        Template temp = freemarkerCfg.getTemplate("exception.tpl");
        temp.process(root, sw);

		return sw.toString();	
	}
	
	public static String complexDataResult(String jobId, Instant expiration, String outputId, String result) throws TemplateException, IOException {
//		jobId
//		expiration
//		outputId
//		result
		Map<String, String> root = new HashMap<>();
		root.put("jobId", jobId);
		root.put("expiration", expiration.toString());
		root.put("outputId", outputId);
		root.put("result", result);
		
		StringWriter sw = new StringWriter();
		
		Template temp = freemarkerCfg.getTemplate("complexDataResult.tpl");
		temp.process(root, sw);
		
		return sw.toString();	
	}
	
	
	
	public static void main(String[] args) throws Exception {
		
		System.out.println(exception("asd"));
		
	}
}
