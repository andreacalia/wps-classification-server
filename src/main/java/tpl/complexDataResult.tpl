<wps:Result
  xmlns:ows="http://www.opengis.net/ows/2.0"
  xmlns:wps="http://www.opengis.net/wps/2.0"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/wps/2.0/wps.xsd">
  <wps:JobID>${jobId}</wps:JobID>
  <wps:ExpirationDate>${expiration}</wps:ExpirationDate>
  <wps:Output id="${outputId}">
  	<wps:Data>
  		${result}
  	</wps:Data>
  </wps:Output>
</wps:Result>