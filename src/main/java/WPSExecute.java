import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.lazy.IBk;
import weka.core.Instances;

public class WPSExecute {

	public static void execute(String request, String token) throws Exception {
		Persistance.setObject(token, JobFactory.buildRunning(token));
		String result = _execute(request);
		Persistance.setObject(token, JobFactory.buildSucceeded(token, result));
	}
	
	public static String executeSync(String request) throws Exception {
		return _execute(request);
	}

	private static String _execute(String request) throws Exception {
		String testInputTxt = WPSClassificator.extractTestData(request);
		String trainInputTxt = WPSClassificator.extractTrainData(request);
		
		WPSDataInput testDataInput = WPSClassificator.parseDataInput(testInputTxt);
		WPSDataInput trainDataInput = WPSClassificator.parseDataInput(trainInputTxt);
		
		Instances testInstances = WPSClassificator.createInstances(testDataInput);
		Instances trainInstances = WPSClassificator.createInstances(trainDataInput);
		
		testInstances.setClassIndex(testInstances.attribute(testDataInput.classIndex).index());
		trainInstances.setClassIndex(trainInstances.attribute(trainDataInput.classIndex).index());
		
		Classifier ibk = new IBk();
		ibk.buildClassifier(trainInstances);
		
		Evaluation evaluation = new Evaluation(trainInstances);
		evaluation.evaluateModel(ibk, testInstances);
		
		return evaluation.toSummaryString();
	}
}
