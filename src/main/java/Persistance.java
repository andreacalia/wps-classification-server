import com.google.gson.Gson;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public final class Persistance {

	private static JedisPool pool = new JedisPool(new JedisPoolConfig(), "localhost");
	
	public static void set(String key, String value) {
		Jedis resource = pool.getResource();
		resource.set(key, value);
		resource.close();
	}
	
	public static String get(String key) {
		Jedis resource = pool.getResource();
		String result = resource.get(key);
		resource.close();
		return result;
	}
	
	public static void setObject(String key, Object obj) {
		Jedis resource = pool.getResource();
		resource.set(key, new Gson().toJson(obj));
		resource.close();
	}
	
	public static <T> T getObject(String key, Class<T> type) {
		Jedis resource = pool.getResource();
		String serialized = resource.get(key);
		resource.close();
		Gson gson = new Gson();
		return gson.fromJson(serialized, type);
	}
	
	public static boolean hasKey(String key) {
		Jedis resource = pool.getResource();
		boolean result = resource.get(key) != null;
		resource.close();
		return result;
	}
}
