
public class Job {

	public String status;
	public String result;
	public String token;
	public String error;

	public Job() {
		super();
	}
	
	public Job(String status, String result, String token) {
		super();
		this.status = status;
		this.result = result;
		this.token = token;
	}
	
	public Job(String status, String token, String result, String error) {
		super();
		this.status = status;
		this.token = token;
		this.error = error;
	}

	@Override
	public String toString() {
		return "Job [status=" + status + ", result=" + result + ", token=" + token + ", error=" + error + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((error == null) ? 0 : error.hashCode());
		result = prime * result + ((this.result == null) ? 0 : this.result.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Job other = (Job) obj;
		if (error == null) {
			if (other.error != null)
				return false;
		} else if (!error.equals(other.error))
			return false;
		if (result == null) {
			if (other.result != null)
				return false;
		} else if (!result.equals(other.result))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		return true;
	}
	
}
