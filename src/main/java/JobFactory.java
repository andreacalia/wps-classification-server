
public final class JobFactory {

	public static Job buildAccepted(String token) {
		Job j = new Job();
		j.status = "Accepted";
		j.token = token;
		return j;
	}
	
	public static Job buildFailed(String token, String error) {
		Job j = new Job();
		j.status = "Failed";
		j.token = token;
		return j;
	}
	
	public static Job buildRunning(String token) {
		Job j = new Job();
		j.status = "Running";
		j.token = token;
		return j;
	}
	
	public static Job buildSucceeded(String token, String result) {
		Job j = new Job();
		j.status = "Succeeded";
		j.token = token;
		j.result = result;
		return j;
	}
}
