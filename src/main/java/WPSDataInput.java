
public class WPSDataInput {
	public String data;
	public String classIndex;

	public WPSDataInput() {
	}

	public WPSDataInput(String data, String classIndex) {
		super();
		this.data = data;
		this.classIndex = classIndex;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classIndex == null) ? 0 : classIndex.hashCode());
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WPSDataInput other = (WPSDataInput) obj;
		if (classIndex == null) {
			if (other.classIndex != null)
				return false;
		} else if (!classIndex.equals(other.classIndex))
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WPSDataInput [data=" + data + ", classIndex=" + classIndex + "]";
	}
}