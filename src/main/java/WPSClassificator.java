import static org.assertj.core.api.Assertions.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;

import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.converters.ConverterUtils.DataSource;

public final class WPSClassificator {

	private final static String TRAIN_INPUT_NAME = "trainData";
	private final static String TEST_INPUT_NAME = "testData";

	private final static String TRAIN_INPUT_START_TAG = "<wps:Input id=\"" + TRAIN_INPUT_NAME + "\">";
	private final static String TRAIN_INPUT_END_TAG = "</wps:Input>";
	private final static String TEST_INPUT_START_TAG = "<wps:Input id=\"" + TEST_INPUT_NAME + "\">";
	private final static String TEST_INPUT_END_TAG = "</wps:Input>";

	private final static String COMPLEX_DATA_START_TAG = "<wps:ComplexData>";
	private final static String COMPLEX_DATA_END_TAG = "</wps:ComplexData>";

	public static Instances createInstances(WPSDataInput input) throws Exception {
		final File testInputFile = createFile(input.data);
        final CSVLoader testLoader = createCSVLoader(testInputFile);
		final DataSource testSource = new DataSource(testLoader);
        return testSource.getDataSet();
	}
	
	public static WPSDataInput parseDataInput(String txtInput) {
		return new Gson().fromJson(txtInput, WPSDataInput.class);
	}

	public static String extractTrainData(String wpsBody) {

		// Find the id of the input
		return extractInputComplexDataPayload(wpsBody, TRAIN_INPUT_START_TAG, TRAIN_INPUT_END_TAG);
	}

	public static String extractTestData(String wpsBody) {

		// Find the id of the input
		return extractInputComplexDataPayload(wpsBody, TEST_INPUT_START_TAG, TEST_INPUT_END_TAG);
	}

	private static String extractInputComplexDataPayload(String wpsBody, String inputStartTag, String inputEndTag) {

		String txt = wpsBody;

		int inputStart = txt.indexOf(inputStartTag) + inputStartTag.length();
		assertThat(inputStart).as("No " + inputStartTag).isGreaterThanOrEqualTo(0);

		// Cut data before Input
		txt = txt.substring(inputStart);

		int inputEnd = txt.indexOf(inputEndTag);
		assertThat(inputEnd).as("No " + inputEndTag).isGreaterThanOrEqualTo(0);

		// Cut data after Input
		txt = txt.substring(0, inputEnd);

		// Find ComplexData
		int complexDataStart = txt.indexOf(COMPLEX_DATA_START_TAG) + COMPLEX_DATA_START_TAG.length();
		assertThat(complexDataStart).as("No " + COMPLEX_DATA_START_TAG).isGreaterThanOrEqualTo(0);

		// Cut data before ComplexData
		txt = txt.substring(complexDataStart);

		int complexDataEnd = txt.indexOf(COMPLEX_DATA_END_TAG);
		assertThat(complexDataEnd).as("No " + COMPLEX_DATA_END_TAG).isGreaterThanOrEqualTo(0);

		// Cut data after ComplexData
		String payload = txt.substring(0, complexDataEnd);

		return payload;
	}

	private static File createFile(final String contents) throws IOException {

		// Need a File for weka to parse it
		File tmpFile = File.createTempFile("input_" + System.currentTimeMillis(), ".csv");
		tmpFile.deleteOnExit();

		BufferedWriter out = new BufferedWriter(new FileWriter(tmpFile));
		out.write(contents);
		out.close();

		return tmpFile;
	}

	private static CSVLoader createCSVLoader(final File input) throws IOException {

		CSVLoader loader = new CSVLoader();
		loader.setSource(input);

		return loader;
	}
	
}
