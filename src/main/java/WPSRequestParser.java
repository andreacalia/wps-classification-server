import static org.assertj.core.api.Assertions.assertThat;

public final class WPSRequestParser {

	private static final String JOB_ID_START_TAG = "<wps:JobID>";
	private static final String JOB_ID_END_TAG = "</wps:JobID>";
	
	public static String extractTokenFromGetStatus(String req) {
		
		int start = req.indexOf(JOB_ID_START_TAG) + JOB_ID_START_TAG.length();
		assertThat(start).as("No " + JOB_ID_START_TAG).isGreaterThanOrEqualTo(0);

		// Cut data before JobID
		req = req.substring(start);

		int end = req.indexOf(JOB_ID_END_TAG);
		assertThat(end).as("No " + JOB_ID_END_TAG).isGreaterThanOrEqualTo(0);

		// Cut data after Input
		return req.substring(0, end);
	}
	
}
